/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shape2;

/**
 *
 * @author bwstx
 */
public class Rectangle extends Shape {

    private double width;
    private double length;

    public Rectangle(double width, double length) {
        super("Rectangle");
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double calArea() {
        return width * length;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width = " + width + ", length = " + length + '}';
    }

}
